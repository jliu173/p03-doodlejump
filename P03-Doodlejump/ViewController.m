//
//  ViewController.m
//  P03-Doodlejump
//
//  Created by 刘江韵 on 2017/2/18.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"bg.jpg"] ];
    // Do any additional setup after loading the view, typically from a nib.
}

@end
